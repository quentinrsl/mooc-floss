import {dirname} from 'path';
import {fileURLToPath} from 'url';
import fs from 'fs';
import {promisify} from 'util';
import path from 'path';

import SvgSpriter from 'svg-sprite';

const asyncReaddir = promisify(fs.readdir);
const asyncReadFile = promisify(fs.readFile);
const asyncWriteFile = promisify(fs.writeFile);
const asyncMkdir = promisify(fs.mkdir);

const log = (...args) => console.log(...args);

const __dirname = dirname(fileURLToPath(import.meta.url));

const generateSprite = async function generateSprite(iconsDir) {
  const spriter = new SvgSpriter({
    dest: path.resolve(__dirname, '../../static/build'),
    shape: {id: {generator: 'icon-%s'}},
    mode: {
      inline: true,
      symbol: {
        dest: path.resolve(__dirname, '../../src/assets/img/svg/sprite/build'),
        sprite: 'icons.symbol.svg.svelte',
      },
    },
    svg: {
      xmlDeclaration: false,
      doctypeDeclaration: false,
    },
  });
  const svgIconsDir = path.resolve(__dirname, iconsDir);
  const paths = await asyncReaddir(svgIconsDir);

  await Promise.all(
    paths
      .map((fileOrDirPath) => path.resolve(svgIconsDir, fileOrDirPath))
      .filter((fileOrDirPath) => !fs.lstatSync(fileOrDirPath).isDirectory())
      .map(async (filePath) => {
        const contents = await asyncReadFile(filePath, {encoding: 'utf-8'});

        spriter.add(filePath, null, contents);
      }),
  );

  return new Promise((resolve, reject) => {
    let results = [];

    spriter.compile(async (err, result) => {
      const modes = Object.keys(result);

      try {
        await Promise.all(
          modes.map(async (mode) => {
            const resources = Object.keys(result[mode]);

            await Promise.all(
              resources.map(async (resource) => {
                const {path: filepath, contents} = result[mode][resource];
                const dir = path.dirname(filepath);

                await asyncMkdir(dir, {recursive: true});
                await asyncWriteFile(filepath, contents);

                results = results.concat(filepath);
              }),
            );
          }),
        );

        resolve(results);
      } catch (err) {
        reject(err);
      }
    });
  });
};

(async function () {
  try {
    const paths = [path.resolve(__dirname, '../../src/assets/img/svg/sprite/icons')];
    const results = await Promise.all(paths.map(generateSprite));

    log(`Generated the following sprites:`);
    log(JSON.stringify(...results, null, 2));
  } catch (err) {
    console.error('Failed to generate sprite:', err);
  }
})();
