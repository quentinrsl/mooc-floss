const { resolve, relative } = require('path');
const { readdir } = require('fs').promises;
const fs = require("fs");
const { Gitlab } = require('@gitbeaker/node');
require('dotenv').config()

async function getFiles(dir) {
    return await getFilesRec(dir, dir);
}

async function getFilesRec(initDir, dir) {
    const dirents = await readdir(dir, { withFileTypes: true });
    const files = await Promise.all(dirents.map((dirent) => {
        const res = resolve(dir, dirent.name);
        return dirent.isDirectory() ? getFilesRec(initDir, res) : relative(initDir, res).replace("\\", "/");
    }));
    return Array.prototype.concat(...files);
}

exports.MergeRequestExercice = class MergeRequestExercice {

    constructor(hookHandler, projectNamespaceId, projectNamespaceName) {
        //Messages to send to the student
        this.messages = {
            INITIAL_COMMIT: "Initial commit",
            ISSUE_TITLE: "`add` function not working",
            ISSUE_DESCRIPTION: "The add function of the project is not working.\nObserved behaviour : `add(1,1) => 0`. Espected behavious : `Hadd(1,1) => 2`",
            PROJECT_NAME: `Auto generated project`,
            PROJECT_DESCRIPTION: `Description for generated project`,
            PROJECT_PATH_PREFIX: `exercice`,
            PIPELINE_SUCCEED: "Looks like the issue is fixed ! Thank you for your contribution !",
            PIPELINE_FAILED: "Oh no ! It looks like your merge request did not pass the unit tests :("
        };

        this.STUDENT_ACCESS_LEVEL = 20;
        this.projectNamespaceId = projectNamespaceId;
        this.projectNamespaceName = projectNamespaceName;
        this.hookHandler = hookHandler;
        this.api = new Gitlab({
            host: "http://" + process.env.HOSTNAME,
            token: process.env.PERSONAL_TOKEN,
        });
        hookHandler.on("merge_request", (e) => this.handleMergeRequest(e));
    }

    /**==================================
    Project initialization
    ====================================*/


    /**
     * Create a hook on the project to listen to merge request events, the callback url is handled by this program
     * @param {Number} projectId The id of the project to initialize
     */
    async _initProjectHooks(projectId) {
        await this.api.ProjectHooks.add(projectId, "http://localhost:" + process.env.PORT + "/hook", { pipeline_events: true, push_events: false, token: process.env.HOOK_SECRET })
    }

    /**
     * Intialize the project with the default files
     * @param {Number} projectId The id of the project to initialize
     */
    async _initProjectFiles(projectId) {
        let commits = []
        let files = await getFiles("default_project")
        for (let file of files) {
            let path = 'default_project/' + file;
            let data = fs.readFileSync(path, 'utf8');
            commits.push({
                action: 'create',
                filePath: file,
                content: data
            });
        }

        await this.api.Commits.create(
            projectId,
            "master",
            tjis.messages.INITIAL_COMMIT,
            commits
        );
    }

    /**
     * Initialize the project with the default issue
     * @param {Number} projectId The id of the project to initialize
     */
    async _initProjectIssue(projectId) {
        await this.api.Issues.create(
            projectId,
            {
                title: this.messages.ISSUE_TITLE,
                issue_type: "issue",
                description: this.messages.ISSUE_DESCRIPTION,
            }
        )
    }

    /**
     * Create the dummy project for the student with id userId
     * @param {Number} userId The id of the user to create the project for
     * @returns {Number} The id of the created project
     */
    async createProject(userId) {
        let res = await this.api.Projects.create({
            namespace_id: this.projectNamespaceId,
            name: this.messages.PROJECT_NAME,
            description: this.messages.PROJECT_DESCRIPTION,
            path: this.messages.PROJECT_PATH_PREFIX + userId,
            visibility: "private",
            forking_access_level: "enabled"
        });
        await this.api.ProjectMembers.add(res.id, userId, this.STUDENT_ACCESS_LEVEL)
        await this._initProjectFiles(res.id)
        await this._initProjectHooks(res.id)
        await this._initProjectIssue(res.id)
        return res.id;
    }

    /**
     * Exercice validation
     */

    /**
     * Test if a merge request made by a student mentions the issue #1 and can be merged 
     * @param {Number} projectId Id of the student's dummy project
     * @param {Number} userId Id of the student (gitlab id) 
     * @param {Number} mergeRequestIid Internal id of the merge request
     * @param {Number} issueIid Internal id of the issue that needs to be fixed
     * @returns an object with a success boolean and a reason string if success is false
     */
    async _isMergeRequestValid(projectId, userId, mergeRequestIid, issueIid = 1) {
        let fixedIssues = await this.api.MergeRequests.closesIssues(projectId, mergeRequestIid)
        let mergeRequest = await this.api.MergeRequests.show(projectId, mergeRequestIid);

        //Test corect author
        if (mergeRequest.author.id != userId) {
            return { success: false, reason: "wrong_author" }
        }

        //Test if the merge request fixes issue #1
        let issueFixed = false
        for (let issue of fixedIssues) {
            if (issue.iid == issueIid) {
                issueFixed = true;
                break;;
            }
        }
        if (!issueFixed) {
            return { success: false, reason: "issue_not_fixed" }
        }
        //Test if merge request is ready to merge
        if (mergeRequest.merge_status != "can_be_merged") {
            return { success: false, reason: "merge_not_ready" }
        }

        return { success: true };
    }

    /**
     * Test if the pipeline of a merge request has succeeded
     * @param {Number} projectId The id of the project
     * @param {*} mergeRequestIid  The internal id of the merge request
     * @returns an object with a success boolean and a reason string if success is false
     */
    async _hasPipelineSucceeded(projectId, mergeRequestIid) {
        let mergeRequest = await this.api.MergeRequests.show(projectId, mergeRequestIid);
        //Test if merge request passed pipeline
        if (mergeRequest.head_pipeline.status != "success") {
            if (mergeRequest.head_pipeline.status == "failed") {
                return { success: false, reason: "pipeline_failed" }
            } else {
                return { success: false, reason: "pipeline_not_ready" }
            }
        } else {
            return { success: true }
        }
    }

    /**
     * Accept a merge request 
     * @param {Number} projectId Id of the project to merge the merge request in
     * @param {Number} mergeRequestIid Internal id of the merge request
     */
    async _acceptMergeRequest(projectId, mergeRequestIid) {
        await this.api.MergeRequests.accept(projectId, mergeRequestIid, { merge_when_pipeline_succeeds: true })
    }

    /**
     * Callback function called when the student completes the activity, add a note to the merge request to notify the student
     * @param {Number} projectId ID of the project
     * @param {Number} mergeRequestIid Merge request internal ID
     * @param {Number} authorId ID of the student
     */
    studentSucceedHandler(projectId, mergeRequestIid, authorId) {
        this.api.MergeRequestNotes.create(projectId, mergeRequestIid, this.messages.PIPELINE_SUCCEED);
        console.log("Student " + authorId + " succeeded");
    }

    /**
     * Function called when the pipeline fails, add a note to the merge request to notify the student
     * @param {Number} projectId Id of the project
     * @param {Number} mergeRequestIid Internal id of the merge request
     */
    pipelineFailHandler(projectId, mergeRequestIid) {
        this.api.MergeRequestNotes.create(projectId, mergeRequestIid, this.messages.PIPELINE_FAIL);
    }

    /**
     * Handles all merge request events
     * @param {Object} event event object passed by the gitlab webhook handler
     * @returns 
     */
    handleMergeRequest(event) {
        console.log(event)
        if (event.payload.project.namespace != this.projectNamespaceName) {
            console.log("wrong namespace")
            return;
        }
        let authorId = event.payload.user.id;
        let projectId = event.payload.object_attributes.target_project_id;
        let mergeRequestIid = event.payload.object_attributes.iid;
        console.log(authorId, projectId, mergeRequestIid, event);

        //Waits 5 seconds to let the pipeline run
        setTimeout((async () => {
            if (await this._isMergeRequestValid(projectId, authorId, mergeRequestIid)) {
                console.log("ready to merge")
                let res = await this._hasPipelineSucceeded(projectId, mergeRequestIid)
                if (!res.success && res.reason == "pipeline_failed") {
                    console.log("pipeline failed")
                    this.pipelineFailHandler(projectId, mergeRequestIid);
                } else if (res.success) {
                    console.log("pipeline success")
                    await this._acceptMergeRequest(projectId, mergeRequestIid);
                    this.studentSucceedHandler(projectId, mergeRequestIid, authorId)
                }
            }
        }), 5000);

    }
}