from code import add
import unittest

class TestProgram(unittest.TestCase):
    def test_add(self):
        self.assertAlmostEqual(add(1,1), 2)
        self.assertAlmostEqual(add(125,1000), 1125)
        self.assertAlmostEqual(add(0,1000), 1000)
        self.assertAlmostEqual(add(15,-10), 5)
        self.assertAlmostEqual(add(1.234,-1), 0.234)

if __name__ == '__main__':
    unittest.main()
