const http = require('http')
const createHandler = require('node-gitlab-webhook')
require('dotenv').config()
const { MergeRequestExercice } = require("./mergeRequestExercice");

let handler = createHandler([{ path: "/hook", secret: process.env.HOOK_SECRET }])

http.createServer(function (req, res) {
	handler(req, res, function (err) {
		res.statusCode = 404
		res.end('no such location')
	})
}).listen(process.env.PORT)

handler.on('error', function (err) {
	console.error('Error:', err.message)
})

let exercice = new MergeRequestExercice(handler, process.env.NAMESPACE_ID, process.env.NAMESPACE_NAME);