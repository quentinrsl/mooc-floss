const readline = require('readline');
const http = require('http')
const createHandler = require('node-gitlab-webhook')
require('dotenv').config()
const { MergeRequestExercice } = require("./mergeRequestExercice");

// Create the dummy project for a student
// The student ID is passed as a command line argument

let handler = createHandler([{ path: "/hook", secret: process.env.HOOK_SECRET }])
let exercice = new MergeRequestExercice(handler, process.env.NAMESPACE_ID, process.env.NAMESPACE_NAME);
exercice.createProject(Number(process.argv[1]));

