# "Lean to use gitlab" activity
This program is a WIP project to create an activity to teach how to use gitlab.
To learn more about this see issue #151

## How to install

### Requirements
* A gitlab instance with admin access
* Node js (not tested for versions anterior to v18.13.0)

### Gitlab Setup
This tool needs a gitlab hook to listen for merge request events.
To setup the hook, go to the admin section of your Gitlab instance, under the System hook menu create new hook. In the URL section put the url where this tool will be available, put a secret token in the secret token section that you will need to provide to this tool. Set the hook to only trigger on merge request events.

You also need to create a new Gitlab group which will be used to create the projects visible to the students. You will need the name and the ID of this group which you can find under the Overview/Group section of the Gitlab admin page for late.

You need to create a new Gitlab user which has all permissions over this group. This user will be the bot the learners will interact with. Log in under this user, go to the user preferences and under access token create a new token (at the moment this software is only tested with all scopes selected). You will need this token for the setup

### Tool setup

In this folder create a `.env` file containing the competed template provided below
```
#Host name of the gitlab instance
HOSTNAME=
#Port of this tools's web server to communicate with gitlab
PORT=
#Secret provided at the creation of the hook
HOOK_SECRET=
#Access token of the bot
PERSONAL_TOKEN=
#ID and name of the namespace containing the student projects
NAMESPACE_ID=
NAMESPACE_NAME=
```

Create the learner gitlab user, copy the newly created user ID in the administration page of Gitlab under the users Overview/Users
Then you can test the creation of a new project linked to an the user with `node init.js USER_ID`.
Then you can start the actual tool with `node index.js`

## How to use
* Login in your gitlab instance with a user which has been previously setup.
* You should see only one project which is read only
* Fork the project, make a commit fixing the issue in src/code.py
* Make a merge request to the original project, if the pipeline suceed you should see a sucees note on your merge request, and the merge request shoud be accepted
* If the pipeline does not pass, the merge request should be closed with a note (This could be improved by not closing the merge request and letting the learner modify the merge request until it passes)
