# Brainstorm - Module 1 - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1617036991567

Marc: 00:00:01.069 Okay. Okay. So welcome, everyone. So the goal of this session is to make descriptions of all units the students will get through during the first week. So normally you've all read the transcript of the descriptions about the first week.

Xavier: 00:00:30.366 Yes, I did.

Marc: 00:00:31.301 Did you?

Xavier: 00:00:32.490 Yeah.

Marc: 00:00:36.352 Okay. Kendall and Ildiko, did you have time for that?

Kendall: 00:00:40.017 I very briefly glanced over them, but I definitely didn't go in as detail as I probably should have.

Ildiko: 00:00:50.747 Me neither. Sorry.

Kendall: 00:00:54.607 Oh, man!

Xavier: 00:00:56.764 Yeah, they are pretty [long?].

Ildiko: 00:00:57.734 [crosstalk] not having the thing on the calendar, so I don't remember to prepare for it.

Xavier: 00:01:04.305 Yeah. Again, sorry about that. That's my fault. And it's true that they are pretty long. So if you haven't opened it and at the last minute tried to open that, that's definitely, yeah, a tricky bit. But I guess we'll do better next week. That's all.

Kendall: 00:01:19.310 Oh, yeah, for sure. It was on my to-do list. But because I didn't see that thing on the calendar, I was like, "Well, maybe I'll just briefly look it over." Yeah. Do you have the link to the transcript really quick? And I can have that open, and we can kind of chat parallel to looking at it.

Marc: 00:01:36.021 Yup. Let me put it in the chat. Yup. Done.

Kendall: 00:01:38.709 Oh, perfect. Thank you. Okay. Yeah.

Marc: 00:01:43.755 Okay. So if I can summarize, the idea of the first week is to not greet people, but that's part of the introduction, but sort of starting to talking about the matter of collaborating to open-source project from the angle of collaboration in general. So basically, there were three main things we talked about when we did brainstorm which were basically the open-source/free software award, the collaboration in general, and the interaction with open-source projects. So what we discussed about collaboration part was mostly to give them a sense of what it means to collaborate. We could use a video game like collaboration building with video game. So the main one we discussed was Minetest which is open-source Minecraft clone. So the idea would be to first make a sort of introduction to collaboration through this game, video presenting why we do that first, then sending them into the game so that they come in a world that already exists with already existing stuff in the world like when you come in a business project. You have plenty of things existing, and you have to fit in and do things that fit into the universe you're in. And this universe is not yours. You're entering it, and you're discovering it as you edit it, basically.

Marc: 00:03:43.224 So we can draw a few parallels here with open-source collaborations. Yeah, so the idea is, basically, to put them into a multiplayer world and let them edit it or play with it or guide them into doing some stuff, ideally, and then we do a recap video, so basically, "Here is what you lived in this world. Here is what it means what you will live in the real world." There are differences, of course. The main difference is that in the video game you don't have-- well, by default, there are no permissions to do stuff and what you do, it's immediately done and not merged with a system of merged request, client review, etc., of course. But there are similar difficulties like what to do and how to do it, basically. So when you come in to a building world, you're not sure what you want to build or what people want you to build. You have to find projects decide that. And that completely mirrors what they will have as a problem later and how to interact with other people. There are people building a house here, "How do I fit in? How do I discuss with those people?" etc. Even the question of communication channels is a real question because people will probably chat in the game, but it's not as efficient as setting a parallel communication channel like Discord slash whatever. Yeah. And so we can debrief all those things afterwards.

Kendall: 00:05:33.962 Yeah, okay. I mean, yeah, I really like the idea of doing a collaborative sort of game thing. I had never heard of Minetest before. I don't know if you are aware, but originally many iterations of Upstream Institute [a go?], there was this introductory exercise in which we would have people build a house of Legos. You knew that, didn't you? Okay. I figured.

Marc: 00:06:03.234 Yeah.

Xavier: 00:06:03.885 Yeah, that came from the [low?] [inaudible] thing. Yeah.

Kendall: 00:06:06.766 Yeah. I was pretty sure that it was him. But yeah, so I think that's a good way to kind of bring ninja people into learning how to work together on something, giving them a goal like building a house, or you could mix it up from week to week or group to group too and be like, "Oh, you need to build a farm," or whatever. That would be good. But yeah, I like that idea, and the digital aspect of it being Minetest is nice.

Marc: 00:06:48.309 So I think we have a third difficulty here which is how to make people who are not familiar with 3D video games involved in a way that they don't feel excluded from the course right away because it's sort of the start. So you could say, "Oh, you just have to start this game." So it should run in all platforms. But if you've never played a video game with WSQD+ mouse, it's a high starting bar for something that it's only remotely related to the course itself.

Kendall: 00:07:25.640 Yeah.

Ildiko: 00:07:32.682 How big of a task do we want to give people?

Marc: 00:07:38.865 I don't think we should give difficult tasks on this, just make a house or make a new building [crosstalk]--

Remi: 00:07:46.712 [crosstalk].

Marc: 00:07:47.877 --that fits into an environment.

Remi: 00:07:51.683 So, Mark, we tried a little bit to play with Minetest. What is the name again?

Marc: 00:08:00.231 Minetest.

Ildiko: 00:08:00.549 Minetest, yeah.

Remi: 00:08:01.473 Minetest. So at first it was a-- because I have a French keyboard, it was a difficult-- it was difficult just to map the keyboard to-- how to move into the game and etc. And then we found that there exists some modification of the game to have a kind of tutorial. It was quite an advanced tutorial, but still I think we should do a kind of tutorial for very beginners, people that are not, as you said, Mark--

Marc: 00:08:46.765 Familiar.

Remi: 00:08:48.331 --yeah, not familiar with the 3D gaming and stuff. And it's always difficult to move around with the [track?] paddle also. Because if you have a mouse, it's easier, I think. But anyway, so we should definitely have something really simple and maybe for those of them for-- yeah. And also, we were asking how do we make sure that people-- because they are able to destroy everything, and the default tool that you have is destroying everything. [laughter] So [inaudible] anywhere, then you destroy everything rather than trying to construct something. So the default, whenever you first launch the game and do something, you will destroy everything. But apart from that, I think we could do a very simple task, as you said, Mark, build a very simple house with a few blocks like this with a certain shape and make a picture of it for screen and put the picture somewhere.

Kendall: 00:10:07.747 Yeah.

Remi: 00:10:08.285 That would be the activity, yeah.

Xavier: 00:10:10.199 And I think for the [crosstalk]--

Xavier: 00:10:10.726 And maybe take a house already existing and modify it, or a little bit more advanced to say that-- you collaborate with other people, so you should take what others did and do something more with it.

Ildiko: 00:10:34.945 I have a quick question. I never played this game. So is the idea to send a group of people into the same game instance, or there's a new person joining into the course and ends up in this game and there are already others there?

Xavier: 00:11:00.187 That was going to be one of my points, is that we'd probably have to handle the asynchronous aspect, even for cross-sessions that started the same time. Maybe not everyone will be in the game at the same time. So there might need to be a process around things for when people are not around. At the same time, there might be also moments where other people are around, and then we can have something synchronous. But it also is maybe something that reflects what happens in projects. Sometimes you'll get immediate response; sometimes you won't, so yeah.

Kendall: 00:11:33.155 Yeah. I was just about to say that depending on the time of day and what project you're working on, everyone might be asleep. And you're just working on your own and trying to fit your pieces in wherever, and you don't really have a whole lot of feedback until they wake up. And then other times, you're trying to place a piece and a 100 other people are at the same time, and that's open source for you. [laughter] Oh, yeah.

Ildiko: 00:11:57.619 Okay. But--

Remi: 00:11:58.459 [crosstalk] every time you--

Ildiko: 00:12:00.501 --who are the maintainers in this game? People who are the mentors or teachers or us or?

Xavier: 00:12:10.836 I think for me, on this, one of the interesting bit is it allows to easily have a little bit of the two roles. It's that you can build something so that becomes your object, your open-source project. And you also get to modify someone else's project, and someone else might come to modify yours. So you get to see the different perspective which is not-- I think one of the big mistakes that a lot of people do when they first contribute to open source is that they are like, "Okay. I'm the contributor, so I'm coming with the really good contribution. They should really thank me for all of that. And I don't have to care much about this. I just send my [pull?] request and done." And I think having a little bit of the other perspective like, "I built something, and someone comes and tries to change it. Maybe I won't like that change," and having a little bit of that perspective from the low cross-perspective of just a game might help when it's about real contributions afterwards.

Ildiko: 00:13:16.160 Okay. I think I probably should go back and read that transcript. No. I'm just trying to understand if this is something that would be still a guided thing, or this is something that runs online and someone finds this course and then finds this step and enters a game that's going forever, sort of.

Remi: 00:13:46.995 Yes.

Ildiko: 00:13:48.303 Yeah.

Xavier: 00:13:48.989 So a game would be going forever, sort of?

Ildiko: 00:13:52.327 And do we have people who are supervising, at least, I don't know, periodically? Or it's just a random game going forever, and then people will get whatever they get out of it?

Kendall: 00:14:08.097 Yeah. I mean, you don't need people involved monitoring all the time because it's-- man, how do I explain it?

Remi: 00:14:25.884 The worst that could happen is that someone enters and destroy everything. And you--

Marc: 00:14:33.104 And it takes lots of effort.

Remi: 00:14:35.317 Yeah. It takes lots of effort because I don't know if you can have a tool that destroys, like a nuke bomb that explodes everything?

Marc: 00:14:42.242 There are small TNT blocks that wipes 10-meter radius.

Remi: 00:14:48.593 Okay. So you could do a big damage. But anyway, I don't know what happens if you really destroy everything. Do you have - what? - water or?

Marc: 00:14:56.252 The world is infinite. It's [procedurally?] generated, so you just build something further and--

Remi: 00:15:04.317 All right. [crosstalk].

Marc: 00:15:05.034 Also, since it's should be creative mode, then everyone coming has an infinity of all possible blocks, so they could rebuild anything fairly quickly.

Remi: 00:15:17.785 And also, I think that every time a person enters the game, it enters the game at the same point in space, right? [crosstalk].

Marc: 00:15:26.706 Roughly same area.

Remi: 00:15:28.896 Same area. Okay. But the area is quite small, so you are approximately in the same area, I would say. I don't know the size of the area, but.

Kendall: 00:15:39.689 Yeah. So to build your house--

Ildiko: 00:15:41.386 But is there--

Kendall: 00:15:41.973 --you'd want to navigate away and to a clearish area and then build your thing and take your screenshot and be a part of the [crosstalk].

Remi: 00:15:52.377 Also, you could try to find another construction somewhere.

Kendall: 00:15:56.789 Yeah.

Ildiko: 00:15:59.097 Is there communication in the game between people?

Marc: 00:16:02.954 Chat. There is a text-- you can write messages, I think.

Ildiko: 00:16:10.384 Okay. No, I'm just kind of having concerns in the sense that I don't think we can always expect good behavior from people. And I know that we can say that this is all experience that you may or may not face when you enter real community, so to say. But if this is part of a course, then this should be sort of a--

Marc: 00:16:39.707 Monitored.

Ildiko: 00:16:40.508 --guaranteed good experience and maybe monitored. And again, if it's part of a course, then you can't really just say that, "Your experience will be something like this," and then no one really controls that game. Maybe controlling is not the right word but rather supervising or making sure that you're not having a horrible experience out of it just because you met someone who texted you awful things just because they are the trolls of this game and found it online and figured that they will just destroy people as opposed to buildings. And also, how you encourage people to work together in this environment if anyone can enter anytime and then maybe nothing happens to them? I don't know if you understand my concerns or [crosstalk].

Marc: 00:17:37.207 At least in Minecraft servers, corporations happen sort of organically which is people start to build something, and if there are other people at the same place at the same time or later, then they try to build to improve it, basically. So that's what happens in most Minecraft servers I've been on. But there are also trolls like on all online games, of course.

Xavier: 00:18:06.132 I mean, I think this is going to be a recurring concern or lapse throughout the course is that we are asking people to contribute, to interact, both within the course and on external projects. So obviously, we'll need to keep an eye for abusive behaviors. And I guess, we are structuring ourselves kind of like on free-software project, so we'll have to take care of that. I think, generally, assuming the best, but just caring about taking care of the worst, it can be a guiding principle that whenever we see that some abusive behaviors happen, we can't try to take care of that. I'm sure that some of the learners will be willing to jump in and maybe become also more involved. Again, all the mechanism of building a project from scratch, at the beginning you don't necessarily have all the rules, but progressively you try to build them to counter the abusive behaviors and try to help with that. And again, this is something we'll have if at some point we ask to do a pull request against the course itself or between each other's projects and etc. We'll have to look at that and take care of that. But it is also part of the learning to know that this can happen and that we need to work on that as [inaudible] [company?], I guess.

Marc: 00:19:37.035 I would expect that we can ask them to report abusive behavior on the forum, and then we can ban or act accordingly.

Xavier: 00:19:47.441 Yup, for example.

Kendall: 00:19:49.482 Yeah. I expect you to want some sort of code of conduct thing.

Marc: 00:19:53.352 We have the code of conduct for the course itself, but I'm not sure you can have a code of conduct for following the course.

Xavier: 00:20:06.611 You have it. Well, I mean, it depends where it's run, but on [OpenEdit?], in general, when you create an account, you have both privacy policy, blah, blah, blah, and code of honor, something like that that you accept. So that's specific to each instance not to each course. But I guess, also in the course, we could have a page where we say that's what we expect of the students.

Marc: 00:20:32.516 Yep. Especially since we expect people to interact with people of the course. In [Xero?] project, we can ask them, "Here's is how you should behave, and please do not--" we can also have it public so that projects can see that students coming from the course are expected to behave respectfully.

Kendall: 00:20:55.672 Yeah. I think that would be a good idea. Yeah.

Marc: 00:21:02.730 And maybe a procedure to basically contact us if some students come from the course with a bad attitude.

Kendall: 00:21:14.153 Good luck at open source. If you've got a bad attitude, it's not [inaudible]. [laughter]

Ildiko: 00:21:20.658 Okay. I just wanted to make sure that we are thinking of all aspects of this. Anyway, I did attend, I think, the Lego access. It was Upstream University at that time, I think, the OpenStack--

Kendall: 00:21:44.985 Yeah. For the [crosstalk].

Ildiko: 00:21:46.064 --training. So I understood and understand the concept. But to me, as a personal experience, I didn't feel, for instance, the Lego exercise that useful, even in terms of collaborating with each other. And that was at least a supervised environment. And we were in the room with the people. I could see them. So there weren't even concerns in an online environment. But I mean, if we can find a middle ground in terms of providing an environment that we feel is safe for people to join and they may or may not get the experience - that's another question, but at least they do have a chance to try - I think that's good. And if it's something that we don't have to maintain on a daily basis, then I think it can work if we really make sure that, as I said, the environment is safe and people have an idea of what they may or may not get out of it.

Kendall: 00:23:17.076 I mean, I think that all of that can be in the documentation or course material around when this activity is presented like, "Go do the thing. Play around with it. Share your house." And then when we do exercises, we come back together and kind of explain what you were supposed to get out of it if you maybe weren't completely aware of what was happening. I think that's more than doable.

Xavier: 00:23:53.694 Yep. And I think also a lot of the course material will be trial and error. We can see what it gives, put it in front of the students, see how they react, [inaudible]. If it doesn't work, we'll replace it. If we realize that while we do it, something better would work better, we'll-- there is the MVP there, and then we have the [inaudible] iterations [otherwise?].

Kendall: 00:24:20.977 Yeah, there are a lot of collaborative games out in the world that we can try to use as replacements if we find that this isn't working.

Remi: 00:24:37.137 Do you have examples? Because we are searching for some of them that you don't have to install and you could access directly in the browser if possible.

Kendall: 00:24:46.760 Yeah. So off the top of my head, I know there is this site. It is not open source is the unfortunate part. It's called Board Game Arena, and they have a wide variety of games that you can play. The thing with that is that it would require a little bit more coordination because the good collaborative games that I know of-- what's it called? Forbidden Island is one of them. I think it's in the premium section. So I have a premium account. I can go and set up games for people, but that requires a little bit more synchronous coordination than Minetest which could be basically completely asynchronous.

Marc: 00:25:35.942 Yeah. But BGA is board games, right?

Kendall: 00:25:40.262 Yeah. They're [crosstalk].

Marc: 00:25:41.421 So it's small sessions depending if you play Civilization or if you play--

Kendall: 00:25:46.483 Yeah, they are board games that have been digitalized by somebody with a lot of time on their hands and access to the licensing and stuff, but a lot of them that I've played have been translated really well from the original board game to the Board Game Arena. It's just a matter of if it's available for free or if it's on a premium account. And the free ones, you still need an account to play, but it's free obviously, and it's a much smaller selection. I'm trying to think of other collaborative ones besides Forbidden Island.

Ildiko: 00:26:25.824 [crosstalk].

Marc: 00:26:26.862 I think the main difference with Minetest is that what you build or what you collaborate on is not persistent contrary to open source, of course.

Kendall: 00:26:37.688 Yeah.

Xavier: 00:26:39.103 And I think beyond collaboration, ideally, it would be about collaboration building something together, which I think why Minetest is attractive for this. Obviously, it has a more open-ended thing, which is obviously the more tricky bit to coordinate, but yeah.

Kendall: 00:26:57.621 Yeah. It's building something together as opposed to collaboratively working towards a specific goal. And both of them fit with open source, but I think collaboratively building something together is, I don't know, a better metaphor or simile or what have you.

Ildiko: 00:27:25.096 Yeah. And I think the more asynchronous we can get with it, the better, I mean, in terms of coordination like the Board Game Arena version, that works fabulous by the way. I love it. But it [crosstalk]--

Kendall: 00:27:40.339 You're [crosstalk].

Ildiko: 00:27:42.110 To do this exercise as to be fully coordinated, then I think it posts a lot of limitations [crosstalk].

Kendall: 00:27:51.229 Yeah. I think the more hands-off and the more asynchronous, the better in terms of scalability and less effort for those that are trying to run the training and stuff.

Marc: 00:28:13.588 Okay. I realize it's been half an hour, and we haven't talked about the first point. So I don't know if you're all from [a calc?] instance. So I've set up a-- it's a link in the [inaudible] [button?]. It's the second link. [light dot from?] [inaudible] dot org. So it's basically a table with all the steps people will go through. So for the Minetest, it's [inaudible], and the rest we haven't talked about yet. Basically, so after the debrief on Minetest or skip of Minetest if people want to skip it, I do think that for people not used to place-ready games we should provide a video of someone going through the steps of playing basically so that they can see what it's like without struggling with controls. So after that when we debrief and say, "This is what you've been through. This is how we translate to the FLOSS world," I think the idea was to talk about FLOSS itself, so start talking about free [inaudible] [open-source?] software. And an idea we had-- so for this third week modules, we wanted to have interviews of people talking about FLOSS.

Marc: 00:29:45.909 So here's two things we want to talk about in this week. First week is experiences about starting into the open-source world, so basically having people, famous or not famous, talking about how they got involved into FLOSS and what it was like for them to enter this world, and so what people could expect of it and what people felt like at the moment or felt like retrospectively about it. And the other transmission aspect that people could talk about is about history of FLOSS, how it came to be, how it evolved, and how it diverged between free software, open source, etc. And so if I get to do everything that we talked about for this chapter, an important thing we've talked about in the transcript was to give an action item to find an open-source project, any open-source project, even if it's not the one you want to contribute to, and say, "Thanks for this project," to this project, so find a communication channel about this project and say, "Are there any developers of this project?" And if someone answers yes, then, "Thank you a lot for developing this. It's awesome, and I use it," and yep, so basically set up people into a mind where they are engaging with open source without committing and without-- without committing themselves to something but with realizing that the contributors are actual human beings that you can talk to and you can engage with and you can have the simplest interaction possible which is saying, "Thank you for what you do."

Kendall: 00:31:44.505 Yeah.

Marc: 00:31:47.773 And the last thing we discussed about this week was to discuss about the main project of the course, which is committing a fix to a bug and starting to give tips on how to measure the health of a project and if a project is a good fit to start contributing to it, to see if it's not a dead project, if it's possible to contribute to it, if they welcome new contributors, etc., just under high level, start to look at open source projects that exist. Here is a list of lists of open-source projects. And you can take whatever interests you and just see if it will be possible-- if you think it will be possible for you to contribute to it. On a high level perspective, is it dead, are there people in it, and--

Kendall: 00:32:45.825 Right. Are they welcoming new contributors? Are they feature complete and just maintaining right now or what have you.

Marc: 00:32:53.743 Or is it a company project that does not welcome external contributors.

Kendall: 00:32:58.024 Yeah. Is it open core and not actually open source?

Marc: 00:33:02.312 Do you have a CLI to sign or? [laughter]

Kendall: 00:33:04.910 Yeah, exactly, yeah, yeah. Collecting those sorts of tips to have would be good. Yeah.

Marc: 00:33:16.646 So the first thing I told was people talking about starting into FLOSS to-- do you think we should have several people famous, not famous, and what should we convey in these interviews?

Kendall: 00:33:36.042 I think having a variety of people from different projects would be good, also including a variety of roles, too, that you can contribute to open-source projects in ways other than just code like documentation or triaging bugs or even just filing bugs or speaking at conferences - there are ton of different ways to be involved in an open-source community - and yeah, having a few, I would say, five minutes or less videos. And I wouldn't require that they watch all of them or anything like that, but watch a couple or at least one. Yeah.

Ildiko: 00:34:28.750 Yeah. I would also just really look into trying to ensure that we have a diverse group of people who are speaking, so not just in terms of roles but also really trying to get people from different places [crosstalk]--

Kendall: 00:34:46.417 Yeah. Actual [person?] [crosstalk].

Ildiko: 00:34:49.524 --all that.

Kendall: 00:34:50.711 Yeah. Diversity on all the levels in terms of roles, geographic location, language, all of that would be ideal.

Xavier: 00:35:01.590 Yeah. And there were a few things that we had talked about and the frustration around that. One of the elements in terms of diversity would be also people who are not necessarily hard-core free-software people, but it might be interesting to geeks in general who might be from the broader community. So one example I gave-- it doesn't need to be that, but for example, John Carmack is not really a free software person nowadays, but he has originally published some like Quake and etc. as free software. He is also someone who is really interesting for any geek in general, so if we compare between him and MS, MS might be very interesting to someone who is already into free software, while John Carmack might be someone that is more interesting to people who just have touched computers or played games.

Xavier: 00:36:10.838 And also the diversity in that, I think, is that it shows, maybe, how it should be with MS, for example, or the people who are really into that, but I don't know. The perspective on how it is to contribute to an open-source project or to free software might be interesting from also the perspective of people who are slightly more external to it and that might provide also some different perspective. And a second point that was brought in is the fact that often it's really difficult for everyone to get started on contributing to open source. We all hesitate for years, feel like impostors doing it and etc. And I think hearing that from big figures that are really respected, it can also help to feel, "Okay. What I'm feeling right now is normal. Those big guys and women did that and felt that when they were at my position." So I think it can help.

Kendall: 00:37:12.481 Even still feel that way now [laughter] after doing it for five years.

Xavier: 00:37:15.722 Yes. Yeah. That's a really good point. [laughter]

Kendall: 00:37:17.849 Yeah. Yeah. Yeah. I think diversity in whether you're in university still and are doing it, or maybe you are being paid by a company to do it, or maybe you work for some company business organization that uses open source and that's how you're involved. I know a variety of research projects and universities have collaborations, and they will run OpenStack in their lab, and people will use that, not realize that that's an open-source project they could be involved in. But maybe they've been using OpenStack for years and years and years, and just don't know. Yeah. There's many different facets of diversity and how you can be involved.

Xavier: 00:38:15.389 Yup. And while we are talking about the interviews, I think one of the thing that could be useful is actually use the opportunity that we'll be interviewing people to illustrate different facets of the course, the different sections. So that might be one of the [medium?]-- almost one of the last thing to add to the course because once we know all the different areas where it will be useful to include questions, then we can have a big list that does the question [inaudible] of the interviews, then reach out to a lot of people, ask them all those questions, and then use little bits and pieces from those different interviews and different parts because this way we don't have a big list of long interviews to watch but more interludes in the middle of the course that brings different perspective.

Ildiko: 00:39:06.608 Yeah. Kind of an inspiration to people. And I like this idea because I think it also breaks up the course material a little. So people can just kind of tune out of the course and just really concentrate on these interviews and why people are doing it and how it goes and how they got into it and all these good examples. And hopefully, they will find at least one person who they can relate to, and then it might help them to get into the doing part of things as well.

Marc: 00:39:41.788 Definitely.
[silence]

Marc: 00:39:53.749 Okay. The next thing on the list is talking about the history of FLOSS, so the values, things like the [for-freedom source?], Debian. How is it called? The DFSG. Yeah, Debian free software guidelines.

Xavier: 00:40:20.153 Do you mean the Debian software [crosstalk]? Yeah. I remember that for the history, we talked about different elements within the history itself but also why does free software or open source exists or what's the difference between the two. For example, the different approaches and philosophies, the fact that, again, from that diversity point of view, wherever you come from whether you are young, old, from any countries, interest in the business side or more from the development side, there is something complete for everyone to take, whether you're more interested in the concrete result, the philosophy, the impact for the condition of human beings, and etc. So there are lots of different angles, and it's true that the historical perspective provides some insights with that because there is, of course, okay, [inaudible] with his printer and etc. But you also have true history different people and different communities and perspective who also got into open source and free software. And I think all of that might be interesting to present so that people can also relate why are we doing that at all.

Kendall: 00:41:43.656 Yeah. Kind of like context and growth of the area and how there are things that are-- open-core software is not open source. And a lot of companies are like, "Oh, yeah. We do this thing. It's open source. Look." And it's like, "No. No, it's not, though. It's actually open core." Yeah.

Marc: 00:42:06.806 And so things in between.

Kendall: 00:42:09.805 Yeah. All kinds of things in between. Licensing is complicated. I'm so glad I don't have to deal with it [inaudible].

Marc: 00:42:20.306 So yeah, we have to sort of define the notions we want to convey and deduce the things that I missed in the details of this paragraphs. Yeah. So we want a diversity of point of views on this matter, right, so how many videos, how many people. I think we should at least have one video on the free-software side and one video on the open-source side, someone from a business perspective and someone from software freedom perspective.

Xavier: 00:43:01.835 Yes. Which are actually don't necessarily exactly free software versus open source. But I think business versus community might also be an additional thing. I mean, all of those don't necessarily oppose each other, by the way, also, but that is perspectives.

Kendall: 00:43:18.405 Yeah. I think for both section two and three, you probably want to just brainstorm a list of people and cast a very, very wide net because, obviously, people are busy and they might not have time to record things. So the more people you can think of to contact to see if they'll record something, the better.

Xavier: 00:43:47.144 Yep. And also, we don't necessarily need to do everything through interviews. I think we can also explain things also. Maybe the only thing I think would be important is to make sure we try to remain as agnostic as possible in terms of the different approach even if I prefer free software to make sure that we still represent the point of view of open source from the perspective that someone from open source would say it, same thing from the business side. So that's definitely where interviews can be useful because then it's really just that person taking that perspective. But I don't think we should either forbid ourselves to do that as long as we are careful to do that and maybe get it reviewed by someone from that philosophy.

Kendall: 00:44:32.806 Yeah. Yeah, that sounds good.

Marc: 00:44:40.076 Okay. So next thing is go say thanks to any project you like. So I suggest the format here would be more text because it could need more adaptations through time, so it's easier if it's text-based. But don't know what you think about it. So it--

Kendall: 00:44:59.595 Yeah. I think directions or whatever of how to find a project and how to find the way to communicate and stuff. Sounds good.

Marc: 00:45:10.584 And sort of prefer synchronous communication channels because you don't want to spam a mailing list with just a, "Thanks for your awesome project," even if these mails are not-- I would welcome some emails like that from time to time but [laughter]--

Xavier: 00:45:26.041 Yeah. I think so.

Kendall: 00:45:26.539 Yeah, yeah.

Xavier: 00:45:29.869 I mean, for this, I think that it matches well the points that we were raising the first meeting. I think we are a bit afraid of spamming people, so I think it's definitely something good to keep in mind. At the same time, like you said, I think right now the problem in the free-software community is not getting spammed by thank-you notes. So if that becomes a problem, that's actually going to be better than today almost.

Kendall: 00:45:58.109 I think it'll probably help. [laughter]

Xavier: 00:46:00.884 And it could be just maybe a question of, in the activity, to mention to first look at the previous history to make sure that it's not adding 1,000 of them. And hopefully, that won't be the case. And even if it is the case, hopefully they will forgive us for that.

Kendall: 00:46:18.450 Yeah. I have not experienced, in any situation, people getting upset about being thanked. Even if they didn't do the thing, they're like, "Oh, you're welcome. I'll definitely take credit for that." [laughter] So I feel like we don't have a whole lot to worry about here aside from making sure it goes to the right place. Because saying thank you for one project in a different project's channel or something, that would be uncomfortable and unfortunate, so yeah, but that's easy enough to figure out.

Remi: 00:46:53.158 And how do you-- because here, as an activity, you have to go and say thanks to any project you like but not any project but FLOSS project. So how do you make sure that-- how do you make sure that--

Marc: 00:47:12.714 I think we can link to lists of lists of projects. So we will have to make lists of lists of FLOSS projects anyway because they have to find a project to contribute to, so we will have to have this kind of list.

Xavier: 00:47:28.058 And I think we have mentioned that we might even be able to use Wikidata in one of the previous meetings--

Marc: 00:47:35.165 [Maybe?].

Xavier: 00:47:35.713 --to have at least a place of contact and update that. And also, it's a FLOSS projects, agreed, but also let's remember that one of the motivation is to give them a first step to start moving them towards their ultimate goal which is to have a contribution merged in their project of choice. Even if that's not a definitive answer, they could still change it over time if we could tell them to start thinking about a project they would love to contribute to and maybe going first with [inaudible] that because it can be, "Thanks. And by the way, I'm interested in maybe contributing. Is there something easy that I could pick up as a first contribution?" Could [inaudible] be starting that which sometimes take a while actually to get through.

Kendall: 00:48:27.219 Yeah. I would definitely encourage them to think of a project that they want to be involved in and thank that one before deferring to a list and being like, "Yeah. Okay. I've used that thing before."

Remi: 00:48:46.005 But we don't want to influence them.

Kendall: 00:48:49.268 Yeah, exactly.

Xavier: 00:48:52.075 Yeah. And actually in that goal, since the tips for the projects, I think it's about picking the right one, right, so maybe the tip could be before the action item? In [part?] four and five. Would that make sense?

Marc: 00:49:09.457 It was this way, I think, because we got through sort of long video sessions where we just give information. So changing from giving information to asking them to do something did sort of make sense, and we ended up with the tip for the project because I think that will be a recurring thing for all the weeks that we will talk about the fact that they will have to make a contribution by the end of the course. So it seems like a follow-up thing we-- I think we will want to end with that every week. You will have to do that, and don't do it all at the end because you will not have the material possibility to contribute to a project by doing it at the last minute.

Xavier: 00:50:05.144 Oh, I see. Yeah, that could make sense. Maybe in that case, they could still do the previous activity which is to go and say thanks to any project that they like or appreciate, and maybe it could be a follow-up in the tips of the week, by the way, to start preparing. If there is also one that you'd like to contribute to, that might also be a good project to go say thanks and maybe stop to talk about your contributions. Yeah, it could be a second step to keep the first one even smaller.

Marc: 00:50:33.804 Yeah. For me, to say thanks is-- just engage them with doing something that includes other people not from the course, not taking into account anything else, just go create a human interaction with someone not from the course, and that's it. That's already something that keeps them engaged with the course.

Kendall: 00:51:01.024 Yeah. [crosstalk]--

Xavier: 00:51:01.593 Actually, I--

Kendall: 00:51:03.322 Sorry, go ahead.

Xavier: 00:51:03.690 Sorry, go ahead. [laughter] All right. So I'll just say this quickly then. I like the fact that it's a very small one, and that makes me think that it might even be something that could be even done earlier because that's actually a smaller step than some of the initial thing that we do. Even the game, for example, the game might be a bigger one. So I'm wondering if the saying thanks could be the very first thing because that's almost something you do as a user while the contribution and the game might be more involved activity, as you've seen, where you tend to spend a lot of time in them, so it could be maybe a good way to wrap the week. I don't know.

Ildiko: 00:51:45.767 I think communicating to a community or project, that can be very intimidating. So I understand that saying thanks in itself is a small task, but when it comes to actually doing it, it may not be that small. I mean, it might be stressful for people. So from this perspective--

Xavier: 00:52:14.012 It's true.

Ildiko: 00:52:14.712 --I think it would be better after the game--

Kendall: 00:52:18.780 Yeah.

Ildiko: 00:52:18.830 --because the game eases them into this whole, "I'm in an environment where I don't know anyone else or most of the other people who are in it. And once I'm through that step, and I did some building and maybe chatting, then I might be ready for the next step of sending a thank-you note to a mailing list." And I also wonder--

Xavier: 00:52:45.853 No, you're right. Actually.

Ildiko: 00:52:48.701 --if this is kind of a mandatory exercise, just from the perspective of I've been contributing for a while, but if I had to send a thank-you to the OpenStack community because they are so awesome. I would still spend quite some time on it. And I'm not sure I would send it at the end just because, I don't know, sometimes saying thank you is a big step just for a human. So anyway, just my two cents, throwing it out there.

Marc: 00:53:16.649 [crosstalk]--

Xavier: 00:53:16.762 [crosstalk]-- sorry. So I think you're really right. And I understand that because actually the very reason why we have it there is because it's a huge psychological step. So I think you're right that the game is probably better to do first, and there might be more installation steps and whatever. But actually, that might be lower than the psychological step. In terms of whether that is mandatory or not, I mean, at the end, nothing is going to be really mandatory because if they want to stop the course, they will stop it. But in terms of highly recommending it, I think I would try to pass that psychological step now because if they delay that, everything else is also going to be delayed because that's actually one of the hardest thing to do. It's to go there, put yourself up there, and start talking to people, so.

Kendall: 00:54:11.873 Yeah, [crosstalk]--

Ildiko: 00:54:12.152 Yeah, I was [crosstalk].

Kendall: 00:54:13.116 --comfort zone. Sorry, go ahead.

Ildiko: 00:54:15.278 No, I'm also wondering if telling them to do an interaction as a step and then having options. Saying thank you is one or go introduce yourself, so maybe having options. So I'm not saying that they should not do the step of interaction, just kind of maybe have some variations of how that interaction might look like would be good I think.

Marc: 00:54:49.019 Also, here, it's after the starting into FLOSS video so they can have some idea of what contributors look like because they have seen them talking about their first interactions with projects. So they see that they will engage with people that look like people who have talked to them in videos just before, which is I think less intimidating than if you right away start with, "Oh, go talk with some people you've never met or heard about or know how they think or where they come from." So if they heard about FLOSS and FLOSS history just before, then they know who is I'm saying thanks to basically.

Xavier: 00:55:38.432 And all that have talked about that aspect will have added big figures saying, "Yeah, that was super scary for me at first," and etc. So they [will?] know that too.

Marc: 00:55:47.774 Yeah, except everyone says, "Oh, my first interaction with FLOSS was very easy, and I just did my thing and was very confident about it."

Xavier: 00:55:58.444 Well, if that's the case, I'll be very surprised, honestly, because I think--

Marc: 00:56:01.205 Me too. In this case, I will be part of the people interviewed so that they can say, "This was really scary and took me two years, so."

Xavier: 00:56:10.332 Me too.

Marc: 00:56:12.267 Okay. So tips for the project with-- do we want to elaborate on what we'd say about the [end?], of course, project basically? So I think we want to give general tips and whether a project will be a good fit or not. But we don't have to give lots and lots of details because it's a subject of another course. But just remember then that there is a big project at the end of the course, and they cannot do it at the last minute, and they can start to think about project to contribute to and with a few tips to see whether a project is an obvious bad choice or not.

Xavier: 00:57:06.910 Or maybe that could be related to what we ask them to do during that week. So for example, here if the entire thing is about creating an interaction with that community, it could be simply are there frequent places to communicate first because some project don't even have it at all? Do people answer there to other people, to you when you actually talked, and did you manage to get to know a bit someone else? What did they reply? Did they point to pointers as a new contributor? It could be related to that, and we could always extend it a little bit more if necessary, but at least that we talk to them. And I guess, maybe we would need to see what would be the next point because maybe something we need to prepare a bit what they will be trying to work on in the following weeks. It could be related to communication. There is the pure communication, like what we just said, but there is also looking at things like do they answer and communicate when people send merge requests?

Marc: 00:58:20.214 That's the topic of the second week. The [second?] week is GitLab, GitHub. So we can talk about this code communication channels.

Xavier: 00:58:29.320 Yeah. That could be a good segue into the next section.

Marc: 00:58:33.692 Yeah. So I think, [Remi?], I did, what you have learned, recap item.

Remi: 00:58:40.853 [inaudible]. Always at the end of each big session - this is the end of one course - we should always have this.

Marc: 00:58:52.494 Just quick recap, text or video?

Remi: 00:58:56.722 Text. Usually what I do is a list of a checkbox. And you check that you indeed have learned everything in the list.

Marc: 00:59:14.463 And yeah, I suggest we end the module with a quiz like multiple choice questions so that we can evaluate basically whether they've learned what we want them to learn which is quite the norm in MOOC.

Kendall: 00:59:39.733 Yeah.

Marc: 00:59:41.843 Okay. So we are almost at the hour mark, but do we want to fill the person in charge column?

Xavier: 00:59:58.221 Yeah. Maybe it would be worth explaining what are the steps on [that?].

Marc: 01:00:01.223 Yeah. Yes. So yeah, I was discussing this with [Xavier?] before the beginning. So the main idea would be that for each line there would be one person in charge of make it happen, basically. So every line is sort of an independent item which has a context for what happens before and what happens after. So it could be sort of semi-independently taken care of by one person who can collaborate with others of course, but who would be mostly responsible for this part of the course, creating or making - how do you say? - your scenario, describing what should happen in a video, for videos making a list of potential people to contact and questions that could be asked, and things like that item by item. So I think it would be good to make it at-- I think, every line could be a GitLab issue that people could attribute themselves, but I thought that in these meetings we could attribute lines to people that already think that they can take care of a specific thing to do.

Xavier: 01:01:31.789 But then, so do we assign the responsibility for each module each week to a person? And then that can be, of course, several sub-tasks that are dedicated to people, or do you want to really completely break it up?

Marc: 01:01:48.508 I thought it would be better to break it. We can have a person in charge for the whole module, but every line, in my opinion, should have someone saying, "I will do it," or, "I'm not really interested in that part, and I prefer if someone can do it."

Xavier: 01:02:13.000 Okay. Yeah. I think we can do a little bit of both, but I think it might be good to have at least [Foich?] theme if it's not even [Foich?] module like one specific person assigned because. Otherwise, I think if we all try to do a little bit of everything, we have a lot of dilution of responsibility. Yeah.

Marc: 01:02:37.645 Yeah. We can assign one person per chapter or five or six chapters here. I call them chapters, but it was the inspiration, so.

Xavier: 01:02:49.704 And actually, that could be a way to think about it, it's to look at the syllabus and maybe see what we are each most interested in and maybe try to split it this way already.

Marc: 01:03:02.907 At the week level-- at the module level you mean?

Xavier: 01:03:06.215 Yeah, for example because we have one, two, three-- we have six modules plus the intro, conclusion, right? So yeah, that's already more people than we are actually right now. But it could be already something good to know, like what are the general areas where each person are most interested in. And that could help guide also the interests and maybe during each of the following week, there could be one specific person knowing that they will have to coordinate that section. That could be more involving too kind of leading that session or something. I don't know what-- that's how I would approach it, but I also want to know what you would like to do.

Marc: 01:03:56.863 Yeah. Do you think we should assign Loic to this week?

Xavier: 01:04:04.925 [laughter] Yeah. He's not here, so he's perfect. Assign him for it. It's true that he has [crosstalk].

Marc: 01:04:15.080 But I said that because I think he's the one with the most sort of experience about the stuff we discussed in this week. But of course, it [crosstalk].

Xavier: 01:04:27.444 I believe [crosstalk] as well [crosstalk]--

Marc: 01:04:29.852 What do you think about the process, not about, "I want to take care of this week." About the process in general, what do you think we could or should do? How do you see things go forward? Basically, that's the main question?

Kendall: 01:04:51.619 I feel like, in terms of keeping things small and actually making progress on them, it would probably be good to have a GitLab thing for each of the general action items here. So I, in the person in charge column, volunteered to collect names and kind of draft questions for interviews or whatever. I saw an issue for each one of those for people to sign up for. As for figuring out who's going to teach the whole module, I feel like those things can kind of wait until more of the content has settled and we have a better timeline because I feel like people's availabilities will change over time. So keeping it as granular as possible right now, I think is the best way to make progress.

Xavier: 01:05:52.186 Oh, and by the way, I did not mean necessarily doing the teaching itself but at least coordinating that section, then I think probably in each of the section we'll probably have multiple teachers. And I mean, I don't know actually. We haven't really talked about that. But it was more like of a coordination role. Because I mean, by default, that means that, Mark, you'll be going to be doing all the coordination, right? And that's where I think maybe--

Marc: 01:06:16.685 The default state would be that, yeah.

Xavier: 01:06:19.524 Yeah. Yeah.

Kendall: 01:06:23.914 Yeah. I can see how that would be good to have one cat herder. Yeah. I'm not volunteering to do that. I'll go do my part. [laughter]

Marc: 01:06:36.811 Okay. I can coordinate the first week and see how it goes.

Kendall: 01:06:40.373 Okay. We can figure it out [crosstalk] next week [crosstalk].

Marc: 01:06:41.161 But then we will need other people do the second week because--

Kendall: 01:06:46.431 Yeah. Okay. Cool.

Xavier: 01:06:52.837 But actually, if we do that, do people already have preferences? Because that was kind of my main point here that before we start locating small items or even weeks is that having an idea of what each person is interested in and maybe feels as the most skilled in might be useful too because then that will guide a lot of those decisions afterwards.

Kendall: 01:07:16.917 So I think maybe since we're rapidly running out of time and I actually have another meeting I have to jump to, maybe we start next week's conversation with that? And right now we just focus on people signing up for the individual action items? Sorry, Mark, you get to herd cats this week. And then we'll--

Marc: 01:07:35.886 That works. Yes, that works.

Kendall: 01:07:36.946 --figure out the rest of it out next week. Does that sound good?

Marc: 01:07:39.935 Yep. I assigned the Minetest item to me because I'm interested in it, so. But I can assign Loic-- I will assign Mike and Loic because Loic has sort of the most experience in what he tries to convey to students in his experience.

Kendall: 01:07:59.246 I'm sure he has a lot of experience with the third, FLOSS history stuff, and yeah. Cool. Cool. All right.

Marc: 01:08:10.858 So we can put--

Kendall: 01:08:14.736 Yeah. Should I go make the GitLab issue, or are you going to handle that, Mark?

Marc: 01:08:22.427 No, I will do my issues. [laughter]

Kendall: 01:08:25.757 Okay. All right. I will--

Marc: 01:08:28.634 So yeah, I think it would be good if people responsible for other stuff did their issues that they are subscribed to it automatically, and we don't have to put assignees to have notifications.

Kendall: 01:08:42.603 Okay, cool.

Xavier: 01:08:44.182 [inaudible] [say?] one of the topics I'm interested in is the interviews themselves. It's something I've had a lot of pleasure doing in the past, even if they were not necessarily videos. So I mean, I don't need to do that alone. But yeah, that's something I'd like to contribute. And I've already actually put an issue for interviews in the GitLab, although it's not specific to this section. But I guess, I can extend that.

Kendall: 01:09:10.692 Yeah, if you want to email me directly, we can work on that together, if you want.

Xavier: 01:09:15.501 Okay.

Kendall: 01:09:16.528 Cool.

Xavier: 01:09:17.930 All right. Well, I should probably drop. But yeah, we'll see you all next Monday, assuming the calendar [and Mark?] goes out for that.

Remi: 01:09:25.338 Yeah. Thank you, Kendall.

Marc: 01:09:27.395 Thank you.

Kendall: 01:09:27.924 All right. Bye.

Marc: 01:09:29.451 Bye.

Remi: 01:09:30.332 Bye.

Marc: 01:09:33.154 Ildiko, do you have preferences?

Ildiko: 01:09:36.381 In terms of action items or in general?

Marc: 01:09:40.788 Yeah. Oh, both.

Ildiko: 01:09:45.120 Well, I will look over the list. I will not hide it. I really am overloaded, so I will do my best to join the meetings now that they are on my calendar and see which item seems to be small enough to pick up and be able to deliver. I don't want to pick something right now and then not deliver on it until, I don't know, for long. So that's where I am right now, and I kind of also need to go because it's getting late here and I have to deal with the cat. I'm so sorry.

Marc: 01:10:19.859 Okay, thank you for joining. And it's completely understandable here.

Remi: 01:10:27.367 Thank you. Yes.

Marc: 01:10:28.496 Thank you for coming already. Thanks for your inputs and for being around and discussing.

Ildiko: 01:10:35.936 Yeah. Sure. Happy to help. Trying to help as much as I can. I'm just trying not to over-commit because then it never ends well. [crosstalk].

Xavier: 01:10:43.445 Sure. Which is an important thing actually and a contribution that we'll probably need to talk about in the course, so that's definitely a good point.

Ildiko: 01:10:52.500 Yeah, that's an important one because I think everyone kind of just likes to forget about that fact that a day is 24 hours and no one gets more.

Xavier: 01:11:02.715 Yup.

Ildiko: 01:11:04.046 Okay. Well, I'll see you next Monday.

Marc: 01:11:07.514 Yep, see you next Monday.

Remi: 01:11:08.972 All right. Yeah.

Ildiko: 01:11:09.551 Thank you.

Marc: 01:11:11.552 Thank you.

Ildiko: 01:11:11.672 Bye.

Marc: 01:11:12.349 Bye. [Remy?], do you want to take--

Xavier: 01:11:19.956 And I think on my side, I jumped in a bit on the [Kendall's?] section, but another one that would potentially interest me would be the project interaction. But actually, yes, sorry [Remy?], you haven't taken anything, so. I'm just mentioning that because most of the points that are interesting to me are early in the course, and probably later in the course I'm then going to be much more passive, so that's why I want to mention that too.

Remi: 01:11:54.194 Yeah. I'm interested also in Minetest, if I can help and [crosstalk].

Marc: 01:12:00.825 Sure. I [crosstalk].

Remi: 01:12:06.090 And maybe the number four, yep, and six. But six, I will do that at the end.

Marc: 01:12:28.340 Okay. So we are missing the tips. But maybe this one we can delay until-- maybe we can regroup the section items with other things we will tell in the week two about this [inaudible] project so that we know where to put it in tips. I don't know.

Xavier: 01:12:52.320 I was about to say that was one, yeah. I think I communicated poorly on those ones because, yeah, those were the main one in the whole course that I was interested in, but that's okay. We'll do that later. And actually, it's better if I'm not too committed from the beginning.

Marc: 01:13:10.952 Yep, and I'm putting you on it. So I will put you on this last item on every week.

Xavier: 01:13:20.659 Oh, all right. Well yeah, we'll see about that. It will depend on the content of those ones, but I'll be happy to at least participate.

Marc: 01:13:29.800 There's an empty line here.

Remi: 01:13:34.526 So of course, we have to modify this very Excel sheet so that we have more fine-grained elements, yes?

Marc: 01:13:57.098 We can fine-grain it. Or if a line already has a person in charge, we can fine-grain it into the issues as long as we respect the-- if we don't make drastic changes to the expected time. I do not even [inaudible] the expected time, so. How much does it amount to?
[silence]

Marc: 01:14:37.035 It makes almost two hours, and I think that will be more. We are more than anywhere in the half of planned content, I think.
[silence]

Marc: 01:15:11.765 Anything else? So a question would-- oh, yeah, I have two things. How do we think we should convey this table into the Git or into others? If we have to, we try to recreate it, and we link to the spreadsheet.

Remi: 01:15:44.400 Yes, let's link it first, and one day we will integrate it.

Marc: 01:15:49.990 Yeah. When it's stable or when--

Remi: 01:15:50.598 Maybe we can, as I told you before, have snapshot of this regularly in the Git.

Marc: 01:16:04.035 Yeah. And the other thing was the issue [Xavier?] raised about, I think, the website for the course. So I think it's a good idea. I'm not sure what's the best way to do it.

Remi: 01:16:25.875 The GitLab page.

Marc: 01:16:27.280 Yeah, that's the first idea I had too.

Xavier: 01:16:32.250 The GitLab page, you mean a static page like a full HTML page or?

Remi: 01:16:36.766 Yes. [crosstalk].

Marc: 01:16:39.627 Yeah, yeah. A website generated by the GitLab CI hosted on moocfloss.gitlab.io/whatever we want.

Xavier: 01:16:50.226 Yeah. We could potentially do that. Is that going to be a purely static page, or will we want to have stuff like newsletter that gets in there eventually?

Marc: 01:17:01.631 Newsletter are static.

Xavier: 01:17:05.058 Well, the form, I guess, yes. But there might be some interaction in the [back?]. What I'm saying is that often one of the classic choice is to do WordPress or something for the marketing side. Yeah. Are we sure that we won't need more than static pages? That's what I mean.

Remi: 01:17:26.549 Yeah. If we want a form, then we will do it. We will put a form and collect the data somewhere.

Xavier: 01:17:36.585 But what's the somewhere?

Remi: 01:17:39.482 In another server. I don't know. If it is only to go to subscribe to a newsletter, we could do that, I don't know--

Xavier: 01:17:57.811 Okay. I mean--

Remi: 01:17:59.360 --somewhere in the database.

Xavier: 01:18:01.791 I mean, if we just start with the static page, that's probably [inaudible] anyway. And then we can always change if the [deadline?] [crosstalk].

Remi: 01:18:10.832 Yeah, I was thinking about-- I don't know. We don't want to-- we don't want to show the collected emails. So yeah, we will have to put some database somewhere if we [need to?].

Xavier: 01:18:29.676 Okay. Well, we can get there at that point. We don't necessarily need to have that from the get-go. Is it just to have-- [inaudible] is just to have something that could be pretty to people who are not geeks and GitLab. And we don't [inaudible] GitLab basically. So all right, so--

Remi: 01:18:48.334 [Sure?]. So we just put a folder in the GitLab for a static website for the - how do you call that? - front page [crosstalk].

Marc: 01:19:00.491 Yeah. Yeah. Project page.

Xavier: 01:19:01.979 [crosstalk].

Remi: 01:19:03.798 Yeah.

Marc: 01:19:04.895 We're thinking about things generated with Hugo or something like that.

Remi: 01:19:10.115 Yeah, with the pretty template. It could be very nice, very quick.

Xavier: 01:19:17.397 All right. So do you have already some constraints on the content of the page? Because what I could offer is that we do that [inaudible] with some UX designer and the team. They are not quite used to designing a page, but they will, of course, want to know what we want to be set on that page. So is there already things that you know we'll want there? I'm guessing things like, I don't know, the [IMT?] logo, some talks about what we want in there, the content of course, I mean.

Marc: 01:19:55.987 Yeah, mostly what we want to do with a course, so who it's for and where we want to get them. We do some things for people who already know how to program, mostly, and we want to get them to make an actual contribution to another person's source project, so the goal of the course and the public. We probably will have to want a page with people involved like the IMT, who we are from a certain-- all the people who contribute to the project, and how to help maybe if [inaudible] there's a page how to help, how to contribute, if we make those into issues in GitLab and saying, "You can help an issue that you're interested in and just contact the person that took responsibility for this item and see if you can help them." Yeah, apart from that--

Xavier: 01:21:09.965 Yeah. In any case, we can always try to [have?] the content over time. But yeah, that sounds like a good start. We'll probably need to actually write also that content. But I could already get things started with the UX designers just so that they mention what they need and etc., and then we can probably get a few tasks on that end and probably also reuse some of the content that we already have on the GitLab repository. That sounds good.

Marc: 01:21:47.967 All things that are in the transcripts too explains the goal of the course. That's not written other than in the first transcript of the first week.

Xavier: 01:21:58.126 Yeah. Yep, yep. Okay. Sounds good. I'll [give?] the action item to move that forward. And if there's anything they did [inaudible], they will ask the questions directly. Another point that would be important to solve in the near future, because otherwise we'll get too much into already work done before we have solved that, is the business aspect. So we had talked about setting up a dedicated call for that. We put that [inaudible] on the calendars?

Remi: 01:22:35.402 Sure. Yes.

Marc: 01:22:36.939 Yep, sure. If other people are interested in this [aspect?], they can join. So when was it?

Xavier: 01:22:46.841 We haven't decided. That's the thing.

Marc: 01:22:48.099 Okay.

Xavier: 01:22:52.941 So when would it be good for you? For me, the time of this meeting is good. Obviously, the Mondays are taken up.

Marc: 01:23:01.393 Yeah, I have something on Mondays too.

Xavier: 01:23:06.833 I could do--

Remi: 01:23:07.169 [crosstalk] first meeting, and then we will expose everything to the IMT director of some sorts.

Marc: 01:23:21.046 Yeah. Maybe we should invite Anna to represent IMT?

Remi: 01:23:26.570 Sure. Yeah.

Xavier: 01:23:29.836 All right. So well, in terms of availability, I think this week, around that time I have Wednesday, Thursday taken, but Tuesday, Friday are okay, or otherwise next week.

Remi: 01:23:45.483 Yep, the same for me, Tuesday, Friday.

Marc: 01:23:48.446 We might want to coordinate with-- we have a meeting with an Anna tomorrow, [inaudible]?

Remi: 01:23:54.577 Yes.

Marc: 01:23:56.327 At 10:00?

Remi: 01:23:57.656 Yes.

Marc: 01:23:58.984 I'm not sure if you will come because you have something at the same time.

Remi: 01:24:02.912 I know. I know. [crosstalk]. Yeah. [crosstalk].

Marc: 01:24:04.920 Will you be able to go come or?

Remi: 01:24:09.608 Yes.

Marc: 01:24:10.569 Okay. I don't think we can schedule a meeting tomorrow if we tell Anna in the morning that we'd like her to attend. But yeah, I don't think [crosstalk]--

Xavier: 01:24:23.026 All right. [crosstalk]--

Marc: 01:24:25.994 I don't have anything in the evening--

Xavier: 01:24:26.352 I could [crosstalk]--

Marc: 01:24:28.432 --because lockdown.

Xavier: 01:24:31.797 So maybe try for Friday, if that works already for the three of us, at the time of the current meeting. And if not, we can start an email threat or something like that or a ticket to coordinate a time, maybe?

Marc: 01:24:46.246 Okay.

Xavier: 01:24:47.885 A ticket actually might be better because then it's [inaudible] by the other people from the project.

Remi: 01:24:53.406 Yep.

Xavier: 01:24:55.524 All right. That sounds good.

Marc: 01:25:01.143 Okay. Thank you. It's almost 9:00, so I think it's time to-- I think you already--

Xavier: 01:25:06.320 Yeah, it is.

Marc: 01:25:07.254 --came half an hour more than what you wanted, so.

Xavier: 01:25:12.026 Yes. Yeah, definitely. But that was useful, so I'm glad that we did that. All right. See you soon.

Marc: 01:25:17.221 Okay. Okay. Thank you and see you soon, yeah. See you on Friday, probably.

Xavier: 01:25:23.243 Yep. Hopefully.

Remi: 01:25:23.613 See you [crosstalk].

Xavier: 01:25:24.815 Bye-bye.

Remi: 01:25:25.661 Bye.

Marc: 01:25:25.879 Bye.
